import os
from libqtile import bar, widget, layout
from libqtile.command import lazy
from libqtile.config import Group, Match, Screen
from mySettings import terminal
from myTheme import myBase16

def GetASep(spad = 4):
    return widget.Sep(foreground=myBase16.base03,padding=spad,size_percent=70)

def GetWidgets(primary: bool):
    widgets = [
        widget.GroupBox(
            active=myBase16.base04,
            block_highlight_text_color=myBase16.base07,
            disable_drag=True,
            fontsize=30, 
            hide_unused=False,
            highlight_color=[myBase16.base00, myBase16.base00],
            highlight_method="line",
            inactive=myBase16.base03,
            other_current_screen_border=myBase16.base04,
            other_screen_border=myBase16.base04,
            padding=1,
            this_current_screen_border=myBase16.base0E,
            this_screen_border=myBase16.base0E,
            urgent_alert_method="text",
            urgent_text=myBase16.base08,
        ),
        GetASep(),
        widget.CurrentLayout(
            mouse_callbacks={"Button5": lazy.next_layout}
        ),
        GetASep(),
        widget.WindowCount(show_zero=True),
        GetASep(),
        #widget.LaunchBar(
        #    progs=[
        #        ('google-chrome','google-chrome-stable','launch google chrome'),
        #        ('visual-studio-code','code','launche visual studio code')
        #    ],
        #),
        widget.Prompt(),
        #GetASep(),
        #widget.TextBox(text=chr(0xfaac),mouse_callbacks={"Button5": lazy.window.kill}),
        widget.WindowName(
            for_current_screen=True,
            format="{name}",
        ),
        widget.Chord(
            chords_colors={
                'qtile': (myBase16.base08, myBase16.base00),
                'rofi': (myBase16.base0B, myBase16.base00),
                'system': (myBase16.base09, myBase16.base00),
            },
            name_transform=lambda name: name.upper(),
        ),
        GetASep(),
        widget.TextBox(
            text=chr(0xf799),
            fontsize=30,
            foreground=myBase16.base08,
        ),
        widget.CPUGraph(
            border_width=0,
            graph_color=myBase16.base08,
            line_width=1,
            samples=95,
            type='line',
        ),
        GetASep(),
        widget.TextBox(
            text=chr(0xf85a),
            fontsize=30,
            foreground=myBase16.base0C,
        ),
        widget.MemoryGraph(
            border_width=0,
            graph_color=myBase16.base0C,
            line_width=1,
            samples=95,
            type='line',
        ),
        GetASep(),
        widget.TextBox(
            text=chr(0xf6ff),
            fontsize=30,
            foreground=myBase16.base0A,
        ),
        widget.NetGraph(
            border_width=0,
            graph_color=myBase16.base0A,
            line_width=1,
            samples=95,
            type='line',
        ),
        # I can't make the Wttr widget work, and the bottom looks weird
        #GetASep(),
        #widget.GenPollUrl(
        #    json=False,
        #    padding=5,
        #    parse=lambda x: x,
        #    update_interval=30,
        #    url='https://wttr.in/La%20Vergne,%20TN"?u&format=3',
        #),
        GetASep(),
        widget.Volume(),
        GetASep(),
        widget.Clock(format='%I:%M %p')
    ]
    if (primary):
        widgets.extend([
            GetASep(),
            widget.Systray(icon_size=18),
            widget.Spacer(length=3),
        ])

    return widgets

def GetTopBar(primary: bool):
    return bar.Bar(
            GetWidgets(primary),
            36,
            background=myBase16.base01,
        )

def GetBottomBar():
    return bar.Bar(
            [
                widget.TaskList(
                    border=myBase16.base0E,
                    highlight_method="block",
                    icon_size=24,
                    margin_x=0,
                    margin_y=1,
                    max_title_width=300,
                    padding_x=4,
                    padding_y=3,
                    rounded=False,
                    spacing=2,
                    txt_floating=chr(0xf2d2)+" ",
                    txt_maximized=chr(0xf2d0)+" ",
                    txt_minimized=chr(0xf2d1)+" ",
                ),
                GetASep(),
                widget.TextBox(
                    text=chr(0xf303),
                    fontsize=30,
                    foreground=myBase16.base0D,
                ),
                widget.CheckUpdates(
                    colour_have_updates=myBase16.base09,
                    colour_no_updates=myBase16.base0D,
                    display_format="Updates: {updates}",
                    distro="Arch_checkupdates",
                    execute=terminal+" -e paru -Syu",
                    no_update_string="Up-to-date",
                ),
                GetASep(),
                widget.TextBox(
                    text=chr(0xf799),
                    fontsize=30,
                    foreground=myBase16.base08,
                ),
                widget.CPU(
                    foreground=myBase16.base08,
                    format="{load_percent}%"
                ),
                # Nvidia stuff needs to be commented out without Nvidia
                #GetASep(),
                #widget.TextBox(
                #    text=chr(0xf93e),
                #    fontsize=30,
                #    foreground=myBase16.base0B,
                #),
                #widget.NvidiaSensors(
                #    foreground=myBase16.base0B,
                #    foreground_alert=myBase16.base08,
                #    format="{temp}"+chr(0xfa03)
                #), 
                #widget.TextBox(
                #    text=chr(0xfa03),
                #    fontsize=30,
                #    foreground=myBase16.base0B,
                #),
                GetASep(),
                widget.TextBox(
                    text=chr(0xf85a),
                    fontsize=30,
                    foreground=myBase16.base0C,
                ),
                widget.Memory(
                    foreground=myBase16.base0C
                ),
                GetASep(),
                widget.TextBox(
                    text=chr(0xf0ab),
                    fontsize=30,
                    foreground=myBase16.base0A,
                ),
                widget.Net(
                    foreground=myBase16.base0A,
                    format="{down}"
                ),
                widget.TextBox(
                    text=chr(0xf0aa),
                    fontsize=30,
                    foreground=myBase16.base0A,
                ),
                widget.Net(
                    foreground=myBase16.base0A,
                    format="{up}"
                ),
                GetASep(),
                widget.Clock(format='%m-%d %a %I:%M %p')
            ],
            36,
            background=myBase16.base01,
        )

home = os.path.expanduser('~')

myScreens = [
    Screen(
        top=GetTopBar(True),
        bottom=GetBottomBar(),
        wallpaper=home+'/Pictures/your-name-kataware-doki.jpg',
        wallpaper_mode='fill',
    ),
    Screen(
        top=GetTopBar(False),
        bottom=GetBottomBar(),
        wallpaper=home+'/Pictures/your-name-kataware-doki.jpg',
        wallpaper_mode='fill',
    ),
    Screen(
        top=GetTopBar(False),
        bottom=GetBottomBar(),
        wallpaper=home+'/Pictures/your-name-kataware-doki.jpg',
        wallpaper_mode='fill',
    )
]
