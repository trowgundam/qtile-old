#!/bin/bash

# Set powerlimit for my RTX 2080 Ti
sudo nvidia-smi -i 0 -pl 380

# Picom, compositor
picom --experimental-backends &

# Polkit Agent, just using Gnome's, because it really doesn't matter
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# Dunst, notifications
dunst &

# Pamac Tray Icon for update notification, disabled since Qtile has this builtin
#pamac-tray &

# Syncthing Tray, --wait tells it to wait for a systray, sicne Openbox doesn't start up
# quick enough
syncthingtray --wait &

# Barrier for sharing KB/M with other computers
# barrier &

# Start GreenWithEnvy to control my GPU fans, and memory downclock (yay for the silicon lottery)
gwe --hide-window &

# OpenRGB, start it minimized, but we also want to just default to our preferred color
openrgb --mode static --color 0000FF &
(sleep 3 && openrgb --startminimized) &

# Add my default key to my ssh-agent
ssh-add "$HOME/.ssh/id_viriveri" &
