import yaml
from libqtile.log_utils import logger

class Base16Theme:
    def __init__(self, 
                 scheme = "Material Vivid", author = "joshyrobot", 
                 base00 = "202124", base01 = "27292c", base02 = "323639", base03 = "44464d", 
                 base04 = "676c71", base05 = "80868b", base06 = "9e9e9e", base07 = "ffffff", 
                 base08 = "f44336", base09 = "ff9800", base0A = "ffeb3b", base0B = "00e676", 
                 base0C = "00bcd4", base0D = "2196f3", base0E = "673ab7", base0F = "8d6e63"):
        self.scheme = scheme
        self.author = author
        self.base00 = base00
        self.base01 = base01
        self.base02 = base02
        self.base03 = base03
        self.base04 = base04
        self.base05 = base05
        self.base06 = base06
        self.base07 = base07
        self.base08 = base08
        self.base09 = base09
        self.base0A = base0A
        self.base0B = base0B
        self.base0C = base0C
        self.base0D = base0D
        self.base0E = base0E
        self.base0F = base0F

def LoadFromYaml(file_path: str) -> Base16Theme:
    try:
        file = open(file_path, "r")
    except Exception as ex:
        logger.exception(ex)
        return Base16Theme()

    try:
        yDoc = yaml.load(file, Loader=yaml.SafeLoader)
        theme = Base16Theme(yDoc["scheme"], yDoc["author"], 
                            yDoc["base00"], yDoc["base01"], yDoc["base02"], yDoc["base03"],
                            yDoc["base04"], yDoc["base05"], yDoc["base06"], yDoc["base07"],
                            yDoc["base08"], yDoc["base09"], yDoc["base0A"], yDoc["base0B"],
                            yDoc["base0C"], yDoc["base0D"], yDoc["base0E"], yDoc["base0F"])
        return theme
    except Exception as ex:
        logger.exception(ex)
        return Base16Theme()
    finally:
        file.close()