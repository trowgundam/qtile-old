from libqtile import layout
from libqtile.config import Match
from myTheme import myBase16

myFloatLayout = layout.Floating(
        border_focus = myBase16.base0E,
        border_normal = myBase16.base01,
        border_width = 3,
        fullscreen_border_width = 0,
        max_border_width = 0,
        name = 'float')

myLayouts = [
    layout.MonadTall(
        align = layout.MonadTall._left, 
        border_focus = myBase16.base0E, 
        border_normal = myBase16.base01, 
        border_width = 3, 
        change_ratio = 0.02,
        margin = 9,
        max_ratio = 0.8,
        min_ratio = 0.2,
        name = "xtall",
        new_at_current = False,
        ratio = 0.5,
        single_border_width = 0,
        single_margin = 0),
    layout.Max(name = "max"),
    myFloatLayout,
    # layout.Columns(border_focus_stack='#d75f5f'),
    # layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

myFloatingRules = layout.Floating(
    border_focus = myBase16.base0E,
    border_normal = myBase16.base01,
    border_width = 3,
    fullscreen_border_width = 0,
    max_border_width = 0,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class='confirmreset'),  # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(wm_class='ssh-askpass'),  # ssh-askpass
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
    ]
)