#!/bin/bash

resp=$(echo -e "Yes\nNo" | rofi -dmenu -mesg "Do you want to download the base16 theme list?" -p ">" -i)
echo "$resp"