from base16.Base16Theme import LoadFromYaml, Base16Theme

myBase16 = LoadFromYaml("/home/jeff/.config/qtile/current-base16.yaml")

myTheme = dict(
    font='FiraCode Nerd Font Mono',
    fontsize=18,
    padding=4,
    foreground=myBase16.base07,
)