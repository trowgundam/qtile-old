from libqtile import layout
from libqtile.config import Group, Match
from myLayouts import myFloatLayout

myGroups = [Group(i) for i in "123456789"]

myGroupWorking = [
    Group("1", label=chr(0xf8a3)),
    Group("2", label=chr(0xf8a6)),
    Group("3", label=chr(0xf8a9)),
    Group("4", label=chr(0xf8ac)),
    Group("5", label=chr(0xf8af)),
    Group("6", label=chr(0xf8b2)),
    Group("7", label=chr(0xf8b5)),
    Group("8", label=chr(0xf8b8)),
    Group("9", label=chr(0xfab9),layout="float",
        matches=[
            Match(wm_class=["ffxivlauncher.exe","ffxiv_dx11.exe","ffxivboot.exe"]), # FFXIV
            Match(wm_class=["explorer.exe"]), # Wine Desktop
            Match(wm_class=["wow.exe", "world of warcraft launcher.exe", "battle.net.exe"]), # WoW
        ]),
]

def getGroupKey(idx: int) -> str:
    return str(idx+1)
