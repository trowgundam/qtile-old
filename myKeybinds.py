import mySettings
from mySettings import mod
from myGroups import getGroupKey
from libqtile import extension
from libqtile.core.manager import Qtile
from libqtile.config import Click, Drag, Group, Key, KeyChord, Group
from libqtile.command import lazy
from typing import Iterable

@lazy.function
def closeAll(qtile: Qtile):
    for window in qtile.current_group.windows:
        window.kill()

@lazy.function
def closeChord(qtile: Qtile):
    qtile.ungrab_chord()

myKeybinds = [
    # Application Starts
    Key([mod], "e", lazy.spawn(mySettings.browser), desc="Start browser"),
    Key([mod], "d", lazy.spawn(mySettings.fileManger), desc="Start file manager"),
    
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    Key([mod], "comma", lazy.next_screen(), desc="Move to prev screen"),
    Key([mod], "period", lazy.prev_screen(), desc="Move to next screen"),

    Key([mod], "f", lazy.window.toggle_floating(), desc="Toggle floating"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(mySettings.terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "c", closeAll, desc="Kill all windows in current group"),

    Key([mod], "r", lazy.spawn("rofi -show-icons -show drun"), closeChord, desc="Show Rofi"),
    KeyChord([mod, "control"], "r", [
        Key([], "r", lazy.spawn("rofi -show run"), closeChord, desc="Run"),
        Key([], "d", lazy.spawn("rofi -show-icons -show drun"), closeChord, desc="DRun"),
        Key([], "w", lazy.spawn("rofi -show-icons -show window"), closeChord, desc="Window"),
    ], "rofi"),

    KeyChord([mod, "control"], "s", [
        Key([], "r", lazy.spawn("systemctl reboot"), desc="Reboot"),
        Key([], "s", lazy.spawn("systemctl poweroff"), desc="Shutdown"),
    ], "system"),

    KeyChord([mod, "control"], "q", [
        Key([], "r", lazy.restart(), desc="Restart Qtile"),
        Key([], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    ], "qtile"),
]

myMouseBindings = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

def addGroupKeybinds(groups: Iterable[Group], keys: Iterable[Key]):
    for idx, group in enumerate(groups):
        keys.extend([
            # mod1 + letter of group = switch to group
            Key([mod], getGroupKey(idx), lazy.group[group.name].toscreen(),
                desc="Switch to group {}".format(group.name)),

            # mod1 + shift + letter of group = switch to & move focused window to group
            Key([mod, "shift"], getGroupKey(idx), lazy.window.togroup(group.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(group.name)),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            Key([mod, "control"], getGroupKey(idx), lazy.window.togroup(group.name),
                desc="move focused window to group {}".format(group.name)),
        ])
